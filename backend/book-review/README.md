# Back-end for the Book Review App
This back-end will handle the requests for the 
 book review from the front end side.

## Prerequisite
 - Java 11 installed to run the application 
 - docker-compose cli command with the version 1.24.1 > is installed 
 - docker cli command with the version 19.03.8 > is installed 

## How to run 
1. Build a docker image
    ```
    docker build . -t book-review
    ```
2. Run the Book Review app with postgres
    ```
    docker-compose up
    ```
## Swagger API
- Go to the link after following the step ```How to run``` http://localhost/swagger-ui/index.html

package com.dev.buddies.bookreview.service;

import com.dev.buddies.bookreview.dao.BookInfoRepository;
import com.dev.buddies.bookreview.dao.ReviewRepository;
import com.dev.buddies.bookreview.exception.ReviewNotFoundException;
import com.dev.buddies.bookreview.model.BookInfo;
import com.dev.buddies.bookreview.model.Review;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class BookReviewServiceTest {

    @InjectMocks
    BookReviewService bookReviewService;

    @Mock
    private BookInfoRepository bookInfoRepository;

    @Mock
    private ReviewRepository reviewRepository;

    @Test
    void findAll_GivenValidISBNAndUserId_ReturnsReviews() {
        // Arrange
        int isbn = 2;
        int userId = 2;
        List<Review> expected = Collections.singletonList(new Review());
        Mockito.when(reviewRepository.findByBookInfoIsbnAndUserId(isbn, userId)).thenReturn(expected);

        // Act
        List<Review> actual = bookReviewService.findAll(isbn, userId);

        // Assert
        assertThat(actual, equalTo(expected));
    }

    @Test
    void findAll_GivenValidISBN_ReturnsReviews() {
        // Arrange
        Integer isbn = 2;
        Integer userId = null;
        List<Review> expected = Collections.singletonList(new Review());
        BookInfo bookInfo = new BookInfo(isbn, expected);
        Mockito.when(bookInfoRepository.findById(isbn)).thenReturn(Optional.of(bookInfo));

        // Act
        List<Review> actual = bookReviewService.findAll(isbn, userId);

        // Assert
        assertThat(actual, equalTo(expected));
    }

    @Test
    void findAll_GivenValidUserId_ReturnsReviews() {
        // Arrange
        Integer isbn = null;
        int userId = 2;
        List<Review> expected = Collections.singletonList(new Review());
        Mockito.when(reviewRepository.findByUserId(userId)).thenReturn(expected);

        // Act
        List<Review> actual = bookReviewService.findAll(isbn, userId);

        // Assert
        assertThat(actual, equalTo(expected));
    }

    @Test
    void findAll_GivenNullParams_ReturnsAllReviews() {
        // Arrange
        Integer isbn = null;
        Integer userId = null;
        List<Review> expected = Collections.singletonList(new Review());
        Mockito.when(reviewRepository.findAll()).thenReturn(expected);

        // Act
        List<Review> actual = bookReviewService.findAll(isbn, userId);

        // Assert
        assertThat(actual, equalTo(expected));
    }

    @Test
    void findAll_WithEmptyReviews_ThrowsException() {
        // Arrange
        Integer isbn = null;
        Integer userId = null;
        Mockito.when(reviewRepository.findAll()).thenReturn(new ArrayList<>());

        // Act
        ReviewNotFoundException exception = assertThrows(ReviewNotFoundException.class,
                () -> bookReviewService.findAll(isbn, userId),
                "");

        // Assert
        assertThat(exception.getMessage(), equalTo("Reviews for the ISBN: null are not found."));
    }

    @Test
    void add_WithValidIsbnAndReview_ReturnsReview() {
        // Arrange
        int isbn = 1;
        BookInfo bookInfo = new BookInfo(isbn, new ArrayList<>());
        Review expected = new Review(1, bookInfo, "review", 1);
        Mockito.when(bookInfoRepository.findById(isbn)).thenReturn(Optional.of(bookInfo));
        Mockito.when(reviewRepository.save(expected)).thenReturn(expected);

        // Act
        Review actual = bookReviewService.add(isbn, expected);

        // Assert
        assertThat(actual, equalTo(expected));
    }
}

package com.dev.buddies.bookreview.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@Data
@Entity(name = "EndUser")
@Table(name = "end_user")
public class EndUser {
    @Id
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;
}

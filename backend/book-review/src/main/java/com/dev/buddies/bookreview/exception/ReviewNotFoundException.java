package com.dev.buddies.bookreview.exception;

public class ReviewNotFoundException extends NotFoundException {

    public ReviewNotFoundException(Integer isbn) {
        super(String.format("Reviews for the ISBN: %s are not found.", isbn));
    }
}

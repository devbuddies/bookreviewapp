package com.dev.buddies.bookreview.dao;

import com.dev.buddies.bookreview.model.BookInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookInfoRepository extends CrudRepository<BookInfo, Integer>{

}

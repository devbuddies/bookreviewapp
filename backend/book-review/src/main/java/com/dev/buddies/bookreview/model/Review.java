package com.dev.buddies.bookreview.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "Review")
@Table(name = "review")
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "book_info_id", referencedColumnName = "isbn", nullable = false)
    private BookInfo bookInfo;

    @Column(name="text")
    private String text;

    @Column(name="userId")
    private int userId;
}

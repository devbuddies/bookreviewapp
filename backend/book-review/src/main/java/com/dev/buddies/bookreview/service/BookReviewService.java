package com.dev.buddies.bookreview.service;

import com.dev.buddies.bookreview.dao.BookInfoRepository;
import com.dev.buddies.bookreview.dao.ReviewRepository;
import com.dev.buddies.bookreview.exception.ReviewNotFoundException;
import com.dev.buddies.bookreview.model.BookInfo;
import com.dev.buddies.bookreview.model.Review;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookReviewService {

    @Autowired
    private BookInfoRepository bookInfoRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    public List<Review> findAll(Integer isbn, Integer userId) {

        List<Review> reviews;

        if (isbn != null && userId != null)
            reviews = reviewRepository.findByBookInfoIsbnAndUserId(isbn, userId);
        else if (isbn != null)
            reviews = bookInfoRepository.findById(isbn).orElse(new BookInfo()).getReviews();
        else if (userId != null)
            reviews = reviewRepository.findByUserId(userId);
        else
            reviews = (List<Review>) reviewRepository.findAll();

        if (reviews.isEmpty())
            throw new ReviewNotFoundException(isbn);

        return reviews;
    }

    public Review add(int isbn, Review review) {
        BookInfo bookInfo = bookInfoRepository.findById(isbn).orElse(new BookInfo(isbn, new ArrayList<>()));
        bookInfoRepository.save(bookInfo);
        review.setBookInfo(bookInfo);

        List<Review> reviews = bookInfo.getReviews();
        for (Review r: reviews) {
            if (r.getUserId() == review.getUserId()) {
                r.setText(review.getText());
                return reviewRepository.save(r);
            }
        }

        return reviewRepository.save(review);
    }

    public void deleteById(List<Integer> isbns, int userId) {}
}

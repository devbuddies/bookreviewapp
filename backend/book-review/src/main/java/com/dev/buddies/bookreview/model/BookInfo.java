package com.dev.buddies.bookreview.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "BookInfo")
@Table(name = "book_info")
public class BookInfo {
    @Id
    @Column(name="isbn")
    private int isbn;

    @JsonManagedReference
    @OneToMany(mappedBy="bookInfo", cascade = CascadeType.ALL)
    private List<Review> reviews;
}

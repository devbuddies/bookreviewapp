package com.dev.buddies.bookreview.dao;

import com.dev.buddies.bookreview.model.Review;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends CrudRepository<Review, Integer> {
    List<Review> findByUserId(int userId);

    List<Review> findByBookInfoIsbnAndUserId(int isbn, int userId);
}

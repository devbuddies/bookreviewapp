package com.dev.buddies.bookreview.controller;

import com.dev.buddies.bookreview.model.Review;
import com.dev.buddies.bookreview.service.BookReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/")
public class BookReviewController {

    @Autowired
    private BookReviewService bookReviewService;

    @GetMapping(value = "/book-reviews")
    public List<Review> findAll(@RequestParam(required = false) Integer isbn, @RequestParam(required = false) Integer userId) {
        return bookReviewService.findAll(isbn, userId);
    }

    @PostMapping(value = "/book-reviews/{isbn}")
    public Review add(@PathVariable int isbn, @RequestBody Review review) {
        return bookReviewService.add(isbn, review);
    }

    @DeleteMapping(value = "/book-reviews/{isbns}")
    public void deleteById(@PathVariable List<Integer> isbns, @RequestParam int userId) {}
}
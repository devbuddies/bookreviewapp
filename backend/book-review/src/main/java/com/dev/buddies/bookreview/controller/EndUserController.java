package com.dev.buddies.bookreview.controller;

import com.dev.buddies.bookreview.model.EndUser;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/")
public class EndUserController {

    @GetMapping(value = "/users")
    public List<EndUser> findAll() {
        return null;
    }

    @GetMapping(value = "/users/{id}")
    public EndUser findById(@PathVariable int id) {
        return null;
    }

    @PostMapping(value = "/users")
    public EndUser add(@RequestBody String reviews) {
        return null;
    }

    @DeleteMapping(value = "/users/{id}")
    public void deleteById(@PathVariable int id) {}
}

